#### 1) Follow https://linrunner.de/en/tlp/docs/tlp-linux-advanced-power-management.html to install tp_smapi and acpi_call  
#### 2) install hdapsd and make sure it's started on boot, the systemd service (/usr/lib/systemd/system/hdapsd.service) needs to be edited to start hdapsd with `-d sda`
#### 3) Execute autorotate.py on startup.

autorotate.py is edited from rotate.py in https://github.com/borlum/tpRotate

Please report any issues here or contact me at thinkpad-autorotate@swedneck.xyz.